import tensorflow as tf
from tensorflow import keras


model = keras.models.load_model("./model/ai-detect-soil-2022-11-07.h5")


def detect_soil(file_in, confidence_threshold=0.5):
    image = keras.preprocessing.image.load_img(
        file_in, target_size=(200, 200)
    )
    image_array = keras.preprocessing.image.img_to_array(image)
    image_reshaped = tf.expand_dims(image_array, 0)  # Create batch axis

    prediction = model.predict(image_reshaped)
    confidence = 1.0 - float(prediction[0][0])

    soil_detected = confidence >= confidence_threshold

    tags = ['detect-soil'] if soil_detected else []

    output_data = {
        'tags': tags,
        'results': {
            'targetDetected': soil_detected,
            'maxConfidence': confidence
        }
    }
    return output_data
